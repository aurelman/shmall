extends Area2D

export var bullet_speed = 200

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):	
	position += Vector2.UP * delta * bullet_speed

func _on_Visibility_screen_exited():
	queue_free()
