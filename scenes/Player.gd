extends Area2D

export var move_speed = 400
export var fire_rate = 400

var _last_fire_occured = 0

var screen_size

var bullet = preload("res://scenes/Bullet.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var velocity = Vector2.ZERO  # The player's movement vector.
	
	if Input.is_action_pressed("fire"):
		fire()	
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * move_speed
		
	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	
func fire():
	var time = OS.get_ticks_msec()
	if time - _last_fire_occured < fire_rate:
		return
	
	_last_fire_occured = time
	var bullet_shot = bullet.instance()
	bullet_shot.position = position
	get_tree().get_root().add_child(bullet_shot)
	
